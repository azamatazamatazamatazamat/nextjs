import Link from 'next/link';
import {SiBurgerking} from 'react-icons/si';

const Header = () => {
	return (
		<header>
			<div>
				<SiBurgerking/>
			</div>
			{/*<nav>*/}
			{/*	<Link href="/"><a id="link">Домой</a></Link>*/}
			{/*	<Link href="/about"><a id="link">О Нас</a></Link>*/}
			{/*	<Link href="/reviews"><a id="link">Отзывы</a></Link>*/}
			{/*	<Link href="/burgers/"><a id="link">Бургеры</a></Link>*/}
			{/*</nav>*/}
			<nav>
				<Link href="/">Домой</Link>
				<Link href="/about">О Нас</Link>
				<Link href="/reviews">Отзывы</Link>
				<Link href="/burgers/">Бургеры</Link>
			</nav>
		</header>
	);
}

export default Header;